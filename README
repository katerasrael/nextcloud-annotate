Annotate PDFs in Nextcloud

DEPENDENCIES:
svg2pdf
pdftk
gs

Ghostscript (gs) is needed for stripping the signature from signed/uneditable
PDF files, and for page manipulation.

Be careful with svg2pdf, there is a real version, part of svglib (pip install svglib),
and there is a fake one that does not work.

Based initially on https://github.com/strukturag/pdfdraw which was found to
be fundamentally broken. In particular, a completely unnecessary dependency
on a backend server where no dependency is needed in order to accomplish
the job needed.


Installation:

Go into nextcloud/apps/ and run;

sudo -u apache git clone https://gitlab.com/nextcloud-other/nextcloud-annotate pdfannotate
(change apache to the appropriate user account if it differs on your distro)

Go into your nextcloud instance (web browser), User icon --> Apps
Scroll to the bottom of "Your apps" and find "PDF Annotate" and press the
enable button.

Now go back to files and hit the "three dots" menu button beside a PDF and
select "Annotate".

Choose a color and draw mode, draw your annotations, and hit the button
beside the color picker to save the changes back to Nextcloud. Older versions of
your file are available through "versions".


USAGE:

Editing modes are available as icons in the upper-right of the screen.
1) Rotate page counter-clockwise (select the tool, then click on the page to rotate).
2) Rotate page clockwise.
3) Select tool.
4) Checkmark stamp.
5) Reviewed by: stamp.
6) SVG image stamp.
7) Manual editing tools;
   a) Freeform drawing,
   b) Box drawing,
   c) Ellipse drawing,
   d) Line drawing,
   e) Text insertion,
   f) Boxed text insertion.
8) Select color,
9) Save,
10) Exit.

To overlay SVG images (signatures, icons, etc.), select the mode, then drag-and-drop an
SVG file into the box that opens. Then click anywhere on the page to stamp the SVG file
into the document as many times as required.

Select mode can be used to MOVE, DELETE, or RESIZE any inserted item. Drag the item to
move. When selected, use the DEL key to delete, or the +/- keys to resize.

There are also touchscreen controls. Tap an item to select, then tap and hold; drag to
move, or add a second finger and pinch to zoom. Dragging a finger or pinch to zoom
without an item selected will scroll or zoom the entire document.

Save the document before exiting to retain changes. Once you have saved, added elements
become immutable.


PAGE MANIPULATION:

This software allows you to collect pages from multiple other PDF files, and combine them
into one new PDF file.

Collect all of your source PDF files into a single directory, then press the "+" button
to add a new file, and select "New PDF manipulation", set a name, and create it.

Now open a PDF source file and MIDDLE CLICK on each page to add it to the manipulation
project. Repeat for each file to be added. When all pages have been added, click the
.pdfm file to create a pdf with the same name. The pdfm file will NOT be deleted, in
case additional pages will need to be added.

Note that the pdfm file itself is just a placeholder. The project data will be stored in
the server's database.
