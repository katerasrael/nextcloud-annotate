<?php
declare(strict_types=1);

namespace OCA\PDFAnnotate\Controller;

use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataDownloadResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Controller;
use OCP\Files\File;
use OCP\Files\IRootFolder;
use OCP\IConfig;
use OCP\IDBConnection;
use OCP\IRequest;
use OCP\IUserSession;
use SimpleXMLElement;

class ApiController extends Controller {

	private $db;
	private $root;
	private $config;
	private $userSession;

	/**
	 * @param string $AppName
	 * @param IRequest $request
	 */
	public function __construct(
			string $AppName,
			IRequest $request,
			IRootFolder $root,
			IDBConnection $db,
			IUserSession $userSession,
			IConfig	$config) {
		parent::__construct($AppName, $request);
		$this->root = $root;
		$this->db = $db;
		$this->userSession = $userSession;
	}

        /**
         * Save annotated PDF.
	 *
	 * @NoAdminRequired
	 * @NoCSRFRequired
         *
         * @param string $fileId
	 * @param string $data
	 * @param string $userid
	 * @param string $rotation
	 * @param bool $forcerot
         * @return DataResponse
         */
        public function saveFile(string $fileId, string $data, string $userid, string $rotation, bool $forcerot) {

		$file = $this->root->getUserFolder($userid)->getById($fileId)[0];
		if (!$file instanceof \OCP\Files\File) return new DataResponse([], Http::STATUS_NOT_FOUND);

		try {
			$content = $file->getContent();
		} catch(\OCP\Files\NotFoundException $e) {
			return new DataResponse([], Http::STATUS_NOT_FOUND);
		}

		$tmp_path = sys_get_temp_dir() . "/nc_pdfanno-" . getmypid();
		if (!mkdir($tmp_path, 0700)) return new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);

		$data = str_replace("'", "", $data); // remove all single quotes used to get nc to see as string
		$data = str_replace(",<svg", "\*<svg", $data); // replace commas with new lines
		$data = explode("\*", $data); // split the string into an array on new lines
		for ($i = 0; $i < count($data); $i++){
			// paper.js produces bad SVG when the string includes newline characters.
			// This repairs the SVG if a text element with newline characters is found.
			if (strpos($data[$i], "<text x") !== false){
				$xml = new SimpleXMLElement(stripcslashes($data[$i]));
				if ($xml !== false){
					foreach($xml->g->g->text as $textelem){
						if (strpos((string)$textelem, "\n") === false) continue;
						$x = (float)$textelem['x'];
						$spacing = (int)$textelem['font-size'] + 2;
						$string = explode("\n", (string)$textelem);
						$y = 0;
						$dom = dom_import_simplexml($textelem);
						$dom->nodeValue = null;
						foreach ($string as $line){
							$xline = $textelem->addChild("tspan", "$line");
							$xline->addAttribute("x", "$x");
							$xline->addAttribute("dy", "$y");
							$y = $spacing;
						}
					}
					$data[$i] = $xml->asXML();
				}
			}
			$filename = str_pad(strval($i), 8, "0", STR_PAD_LEFT);
			if (file_put_contents($tmp_path . "/$filename.svg", $data[$i]) == false)
				return new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);
		}

		system("svg2pdf $tmp_path/*.svg && pdftk $tmp_path/*.pdf cat output $tmp_path/overlay.pdf", $ret);
		if ($ret != 0) return new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);
		if (file_put_contents($tmp_path . "/base.pdf", $content) == false)
			return new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);

		$rot = explode(",", $rotation);
		$dorot = $forcerot;
		$idx = 1;
		foreach($rot as $val){
			if ($val != 0) $dorot = true;
		}
		if ($dorot){
			foreach($rot as $val){
				$dir = "$idx";
				switch($val){
				case 0:
				case 360:
					$dir.="north";
					break;
				case 90:
					$dir.="east";
					break;
				case 180:
					$dir.="south";
					break;
				case 270:
					$dir.="west";
					break;
				}
				$filename = "rot-".str_pad(strval($idx), 8, "0", STR_PAD_LEFT);
				$cmd = "pdftk $tmp_path/base.pdf cat $dir output $tmp_path/$filename.pdf";
				system("pdftk $tmp_path/base.pdf cat $dir output $tmp_path/$filename.pdf", $ret);
				if ($ret != 0){
					system("gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=$tmp_path/base_b.pdf $tmp_path/base.pdf");
					system("mv -f $tmp_path/base_b.pdf $tmp_path/base.pdf");
					system("pdftk $tmp_path/base.pdf cat $dir output $tmp_path/$filename.pdf", $ret);
					if ($ret != 0) return new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);
				}
				$idx++;
			}
			$cmd = "pdftk $tmp_path/rot-*.pdf cat output $tmp_path/base.pdf";
			system($cmd, $ret);
		}

		$content = `pdftk $tmp_path/base.pdf multistamp $tmp_path/overlay.pdf output -`;
		if ($content === NULL || strlen($content) < 500){
			system("gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=$tmp_path/base_b.pdf $tmp_path/base.pdf");
			$content = `pdftk $tmp_path/base_b.pdf multistamp $tmp_path/overlay.pdf output -`;
		}
		if ($content === NULL || strlen($content) < 500)
			return new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);
		
		system("rm -rf $tmp_path");

		try {	
			$file->putContent($content);
		} catch(\OCP\Files\NotPermittedException $e){
			return new DataResponse([], Http::STATUS_UNAUTHORIZED);
		}

		return new DataResponse([$tmp_path]);
	}
}
