<?php

declare(strict_types=1);

namespace OCA\PDFAnnotate\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\IOutput;
use OCP\Migration\SimpleMigrationStep;

/**
 * Auto-generated migration step: Please modify to your needs!
 */
class Version9Date20210611134431 extends SimpleMigrationStep {

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 */
	public function preSchemaChange(IOutput $output, Closure $schemaClosure, array $options): void {
	}

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 * @return null|ISchemaWrapper
	 */
	public function changeSchema(IOutput $output, Closure $schemaClosure, array $options): ?ISchemaWrapper {
				/** @var ISchemaWrapper $schema */
		$schema = $schemaClosure();

		if (!$schema->hasTable('pdfmanipulator_pages')) {
			$table = $schema->createTable('pdfmanipulator_pages');
			$table->addColumn('pdfm_fileid', 'bigint', [
				'notnull' => true,
				'length' => 20,
			]);
			$table->addColumn('page_fileid', 'bigint', [
				'notnull' => true,
				'length' => 20,
			]);
			$table->addColumn('page', 'integer', [
				'notnull' => true,
				'unsigned' => true,
			]);
			$table->addColumn('order', 'integer', [
				'notnull' => true,
				'unsigned' => true,
			]);
			$table->setPrimaryKey(['pdfm_fileid', 'page_fileid', 'page']);

			$table->addForeignKeyConstraint(
				$schema->getTable('filecache')->getName(),
				['pdfm_fileid'],
				['fileid'],
				['onDelete' => 'CASCADE'],
				'pdfm'
			);

			$table->addForeignKeyConstraint(
                                $schema->getTable('filecache')->getName(),
                                ['page_fileid'],
                                ['fileid'],
                                ['onDelete' => 'CASCADE'],
                                'pagefile'
                        );
		}

		return $schema;
	}

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 */
	public function postSchemaChange(IOutput $output, Closure $schemaClosure, array $options): void {
	}
}
