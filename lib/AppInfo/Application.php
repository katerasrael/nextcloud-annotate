<?php

declare(strict_types=1);

namespace OCA\PDFAnnotate\AppInfo;

use OCA\Viewer\Event\LoadViewer;
use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;

class Application extends App implements IBootstrap {
	public const APP_ID = 'pdfannotate';

	public function __construct() {
		parent::__construct(self::APP_ID);
	}

	public function register(IRegistrationContext $context): void {
	}

	public function boot(IBootContext $context): void {
		\OCP\Util::addScript(self::APP_ID, 'loader');
		\OCP\Util::addStyle(self::APP_ID, 'pdfannotate');
	}
}
