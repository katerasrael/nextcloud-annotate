/* eslint indent: ["error", 2, { "outerIIFEBody": 0, "SwitchCase": 1 }] */

(function() {
"use strict";

var CSS_UNITS = 96.0 / 72.0;
var INITAL_COLORS  = [
  "#ff0000",
  "#008080",
  "#00ffff",
  "#00ff00",
  "#008000",
  "#c0c0c0",
  "#f7347a",
  "#990000",
  "#ccff00",
  "#3399ff",
  "#f6546a",
  "#ffff00",
  "#ffa500",
  "#0000ff",
  "#800080",
];

var PERMISSION_CREATE = 4;
var PERMISSION_READ = 1;
var PERMISSION_UPDATE = 2;
var PERMISSION_DELETE = 8;
var PERMISSION_SHARE = 16;
var PERMISSION_ALL = 31;

var svgStamp = "";

// Taken from https://gist.github.com/jed/982883
function uuidv4() {
  return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  );
}

function getRandomColor() {
  return INITAL_COLORS[Math.floor(Math.random() * INITAL_COLORS.length)];
}

// Required for running in older versions of Qt (see #31).
// https://github.com/uxitten/polyfill/blob/master/string.polyfill.js
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/padStart
if (!String.prototype.padStart) {
  String.prototype.padStart = function padStart(targetLength, padString) {
    targetLength = targetLength >> 0; //truncate if number, or convert non-number to 0;
    padString = String(padString !== undefined ? padString : ' ');
    if (this.length >= targetLength) {
      return String(this);
    } else {
      targetLength = targetLength - this.length;
      if (targetLength > padString.length) {
        padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
      }
      return padString.slice(0, targetLength) + String(this);
    }
  };
}

var uid = uuidv4();
var object_count = 0;

function getObjectId() {
  return uid + ":" + (++object_count);
}

var Storage = function(prefix) {
  this.prefix = prefix;
  this.storage = window.localStorage;
  if (!this.storage) {
    console.warn('LocalStorage not available, saving of settings will be disabled.');
  }
};

Storage.prototype._getKey = function(key) {
  if (!this.prefix) {
    return key;
  }

  return this.prefix + '.' + key;
};

Storage.prototype.get = function(key) {
  if (!this.storage) {
    return null;
  }

  return this.storage.getItem(this._getKey(key));
};

Storage.prototype.set = function(key, value) {
  if (!this.storage) {
    return;
  } else if (!value) {
    this.storage.removeItem(this._getKey(key));
    return;
  }

  this.storage.setItem(this._getKey(key), value);
};

var BaseDrawer = function(annotator, previous_mode) {
  this.annotator = annotator;
  this.previous_mode = previous_mode;
};

BaseDrawer.prototype.destroy = function() {};
BaseDrawer.prototype.activate = function(page_annotator) {};
BaseDrawer.prototype.onMouseDown = function(page_annotator, event) {};
BaseDrawer.prototype.onMouseUp = function(page_annotator, event) {};
BaseDrawer.prototype.onMouseDrag = function(page_annotator, event) {};
BaseDrawer.prototype.onMouseEnter = function(page_annotator, event) {};
BaseDrawer.prototype.onMouseLeave = function(page_annotator, event) {};
BaseDrawer.prototype.onMouseMove = function(page_annotator, event) {};
BaseDrawer.prototype.onClick = function(page_annotator, event) {};
BaseDrawer.prototype.onKeyUp = function(page_annotator, event) {};
BaseDrawer.prototype.onItemMoved = function(page_annotator, name, item, event) {};

var NullDrawer = function() {
  BaseDrawer.apply(this, arguments);
};
NullDrawer.prototype = Object.create(BaseDrawer.prototype);

var FreehandDrawer = function(annotator) {
  BaseDrawer.apply(this, arguments);
  this.path = null;
  this.intervalId = null;
};
FreehandDrawer.prototype = Object.create(BaseDrawer.prototype);

FreehandDrawer.prototype.onMouseDown = function(page_annotator, event) {
  page_annotator.activate();
  this.page_annotator = page_annotator;
  this.path = page_annotator.createPath({
    name: getObjectId(),
    strokeColor: this.annotator.color,
    strokeWidth: 2
  });
  this.path.add(event.point);
};

FreehandDrawer.prototype.onMouseUp = function(page_annotator, event) {
  if (!this.path) {
    return;
  }
  if (this.path.segments.length) {
    this.path.smooth();
  }
  this.path = null;
  clearInterval(this.intervalId);
};

FreehandDrawer.prototype.onMouseDrag = function(page_annotator, event) {
  if (!this.path) {
    return;
  }
  this.path.add(event.point);
};

var LineDrawer = function(annotator) {
  FreehandDrawer.apply(this, arguments);
};

LineDrawer.prototype = Object.create(FreehandDrawer.prototype);

LineDrawer.prototype.onMouseDrag = function(page_annotator, event) {
  if (!this.path) {
    return;
  }
  this.path.removeSegment(1);
  this.path.add(event.point);
};

var TextInsert = function() {
	BaseDrawer.apply(this, arguments);
};
TextInsert.prototype = Object.create(BaseDrawer.prototype);
TextInsert.prototype.onClick = function(page_annotator, event){
	var text = prompt("Text to insert:");
	if (text != null && text.length > 0){
		var textLocation = new paper.Point(event.point.x, event.point.y);
		var paperText = new paper.PointText(textLocation);
		paperText.fillColor = this.annotator.color;
		paperText.content = text.replace(/\\n/g,"\n");
                paperText.onMouseDrag = function(event){
                        this.annotator.drawer.onItemMoved(this, null, paperText, event);
                }.bind(this);
	}
}

var BoxTextInsert = function() {
        BaseDrawer.apply(this, arguments);
};
BoxTextInsert.prototype = Object.create(BaseDrawer.prototype);
BoxTextInsert.prototype.onClick = function(page_annotator, event){
        var text = prompt("Text to insert:");
        if (text != null && text.length > 0){
                var textLocation = new paper.Point(event.point.x, event.point.y);
                var paperText = new paper.PointText(textLocation);
                paperText.fillColor = this.annotator.color;
                paperText.content = text.replace(/\\n/g,"\n");
                var rect = new paper.Path.Rectangle(new paper.Rectangle(paperText.bounds.x - 2, paperText.bounds.y - 2, paperText.bounds.width + 4, paperText.bounds.height + 4));
                rect.fillColor = 'white';
                rect.strokeColor = 'black';
                paperText.insertAbove(rect);
                paperText.onMouseDrag = function(event){
                        this.annotator.drawer.onItemMoved(this, null, paperText, event);
                        this.annotator.drawer.onItemMoved(this, null, rect, event);
                }.bind(this);
        }
}

var ReviewedInsert = function() {
        BaseDrawer.apply(this, arguments);
};
ReviewedInsert.prototype = Object.create(BaseDrawer.prototype);
ReviewedInsert.prototype.onClick = function(page_annotator, event){
	var textLocation = new paper.Point(event.point.x + 2, event.point.y + 14);
	var paperText = new paper.PointText(textLocation);
	var date = new Date();
	paperText.content = "Reviewed by: "+page_annotator.annotator.displayname+"\n"+date.toDateString()+", "+date.toLocaleTimeString('en-US');
	paperText.fillColor = this.annotator.color;
	paperText.fontSize = 14;
	var rect = new paper.Path.Rectangle(new paper.Rectangle(paperText.bounds.x - 2, paperText.bounds.y - 2, paperText.bounds.width + 4, paperText.bounds.height + 4));
	rect.fillColor = 'white';
	rect.strokeColor = 'black';
	paperText.insertAbove(rect);
	paperText.onMouseDrag = function(event){
		this.annotator.drawer.onItemMoved(this, null, paperText, event);
		this.annotator.drawer.onItemMoved(this, null, rect, event);
	}.bind(this);
}

var InputbyInsert = function() {
        BaseDrawer.apply(this, arguments);
};
InputbyInsert.prototype = Object.create(BaseDrawer.prototype);
InputbyInsert.prototype.onClick = function(page_annotator, event){
        var textLocation = new paper.Point(event.point.x + 2, event.point.y + 14);
        var paperText = new paper.PointText(textLocation);
        var date = new Date();
        paperText.content = "Input by: "+page_annotator.annotator.displayname+"\n"+date.toDateString()+", "+date.toLocaleTimeString('en-US');
        paperText.fillColor = this.annotator.color;
        paperText.fontSize = 14;
        var rect = new paper.Path.Rectangle(new paper.Rectangle(paperText.bounds.x - 2, paperText.bounds.y - 2, paperText.bounds.width + 4, paperText.bounds.height + 4));
        rect.fillColor = 'white';
        rect.strokeColor = 'black';
        paperText.insertAbove(rect);
        paperText.onMouseDrag = function(event){
                this.annotator.drawer.onItemMoved(this, null, paperText, event);
                this.annotator.drawer.onItemMoved(this, null, rect, event);
        }.bind(this);
}

var CheckInsert = function() {
        BaseDrawer.apply(this, arguments);
};
CheckInsert.prototype = Object.create(BaseDrawer.prototype);
CheckInsert.prototype.onClick = function(page_annotator, event){
	page_annotator.activate();
	this.page_annotator = page_annotator;
	this.path = page_annotator.createPath({
		name: getObjectId(),
		strokeColor: this.annotator.color,
		strokeWidth: 1.5
	});
	this.path.add(event.point);
	this.path.add(new paper.Point(event.point.x +7, event.point.y + 7));
	this.path.add(new paper.Point(event.point.x +21, event.point.y - 14));
}

var SVGDrawer = function() {
        BaseDrawer.apply(this, arguments);
};
SVGDrawer.prototype = Object.create(BaseDrawer.prototype);
SVGDrawer.prototype.onClick = function(page_annotator, event){
        page_annotator.activate();
        this.page_annotator = page_annotator;
	var svgItem;
	var parser = new DOMParser();
	var xmlDoc = parser.parseFromString(svgStamp, "text/xml");
	var x = xmlDoc.getElementsByTagName("svg");
	x[0].removeAttribute("viewBox");
	var newXML = (new XMLSerializer()).serializeToString(xmlDoc);
        svgItem = page_annotator.scope.project.importSVG(newXML);
	svgItem.position = event.point;
	svgItem.onMouseDrag = function(event){
                this.annotator.drawer.onItemMoved(this, null, svgItem, event);
        }.bind(this);
}

var RotateCCW = function() {
        BaseDrawer.apply(this, arguments);
};
RotateCCW.prototype = Object.create(BaseDrawer.prototype);
RotateCCW.prototype.onClick = function(page_annotator, event){
	console.log("Rotate CCW");
	page_annotator.rotation -= 90;
	if (page_annotator.rotation < 0)
		page_annotator.rotation += 360;
	page_annotator.update(PDFViewerApplication.pdfViewer.currentScale);
}

var RotateCW = function() {
        BaseDrawer.apply(this, arguments);
};
RotateCW.prototype = Object.create(BaseDrawer.prototype);
RotateCW.prototype.onClick = function(page_annotator, event){
	console.log("Rotate CW");
	page_annotator.rotation += 90;
	if (page_annotator.rotation >=360)
		page_annotator.rotation -= 360;
	page_annotator.update(PDFViewerApplication.pdfViewer.currentScale);
}

var RectangleDrawer = function(annotator) {
  BaseDrawer.apply(this, arguments);
  this.options = {};
  this.rect = null;
};

RectangleDrawer.prototype = Object.create(BaseDrawer.prototype);

RectangleDrawer.prototype.onMouseDown = function(page_annotator, event) {
  page_annotator.activate();
  this.options = {
    name: getObjectId(),
    strokeColor: this.annotator.color,
    strokeWidth: 2,
    from: [event.point.x, event.point.y],
    to: [event.point.x, event.point.y],
  };
  this.update(page_annotator);
};

RectangleDrawer.prototype.update = function(page_annotator) {
  var create = page_annotator.createRectangle.bind(page_annotator);
  if (!this.rect) {
    this.rect = create(this.options);
  } else {
    this.rect = this.rect.replaceWith(create(this.options));
  }
};

RectangleDrawer.prototype.onMouseUp = function(page_annotator, event) {
  if (!this.rect) {
    return;
  }
  this.options.to = [event.point.x, event.point.y];
  this.update(page_annotator);
  this.rect.ready = true;
  this.options = {};
  this.rect = null;
};

RectangleDrawer.prototype.onMouseDrag = function(page_annotator, event) {
  if (!this.rect) {
    return;
  }
  this.options.to = [event.point.x, event.point.y];
  this.update(page_annotator);
};

var EllipseDrawer = function(annotator) {
  BaseDrawer.apply(this, arguments);
  this.options = {};
  this.ellipse = null;
};

EllipseDrawer.prototype = Object.create(BaseDrawer.prototype);

EllipseDrawer.prototype.onMouseDown = function(page_annotator, event) {
  page_annotator.activate();
  this.options = {
    name: getObjectId(),
    strokeColor: this.annotator.color,
    strokeWidth: 2,
    from: [event.point.x, event.point.y],
    to: [event.point.x, event.point.y],
  };
  this.update(page_annotator);
};

EllipseDrawer.prototype.update = function(page_annotator) {
  var create = page_annotator.createEllipse.bind(page_annotator);
  if (!this.ellipse) {
    this.ellipse = create(this.options);
  } else {
    var temp_ellipse = create(this.options);
    this.ellipse = this.ellipse.replaceWith(temp_ellipse);
  }
};

EllipseDrawer.prototype.onMouseUp = function(page_annotator, event) {
  if (!this.ellipse) {
    return;
  }
  this.options.to = [event.point.x, event.point.y];
  this.update(page_annotator);
  this.ellipse.ready = true;
  this.options = {};
  this.ellipse = null;
};

EllipseDrawer.prototype.onMouseDrag = function(page_annotator, event) {
  if (!this.ellipse) {
    return;
  }
  this.options.to = [event.point.x, event.point.y];
  this.update(page_annotator);
};

var SelectDrawer = function(annotator) {
  BaseDrawer.apply(this, arguments);
  this.selected_item = null;
};
SelectDrawer.prototype = Object.create(BaseDrawer.prototype);

SelectDrawer.prototype._select = function(item) {
  if (this.selected_item === item) {
    return;
  }

  this._unselect();
  this.selected_item = item;
  this.selected_item.shadowColor = "black";
  this.selected_item.shadowBlur = 10;
};

SelectDrawer.prototype._unselect = function() {
  if (!this.selected_item) {
    return;
  }

  this.selected_item.shadowColor = null;
  this.selected_item.shadowBlur = 0;
  this.selected_item = null;
};

SelectDrawer.prototype.destroy = function() {
  this._unselect();
};

SelectDrawer.prototype.onMouseDown = function(page_annotator, event) {
  var hit = page_annotator.scope.project.hitTest(event.point);
  if (!hit) {
    this._unselect();
    return;
  }

  this._select(hit.item);
};

SelectDrawer.prototype.onClick = function(page_annotator, event) {
  var hit = page_annotator.scope.project.hitTest(event.point);
  if (!hit) {
    this._unselect();
    return;
  }

  this._select(hit.item);
};

SelectDrawer.prototype.onKeyUp = function(page_annotator, event) {
  if (!this.selected_item) {
    return;
  }

  switch (event.keyCode) {
    case 8: // Backspace
      // Fallthrough
    case 46: // Delete key
      var point = this.selected_item.position;
      this.selected_item.remove();
      this._unselect();
      var hit = page_annotator.scope.project.hitTest(point);
      if (hit){
	      hit.item.remove();
      }
      break;
    case 107: // + key
      var hitItems = page_annotator.scope.project.hitTestAll(this.selected_item.position);
      if (hitItems.length < 1) this.selected_item.scale(1.1);
      else hitItems.forEach(function(hit, index){
	      hit.item.scale(1.1);
      });
      break;
    case 109: // - key
      var hitItems = page_annotator.scope.project.hitTestAll(this.selected_item.position);
      if (hitItems.length < 1) this.selected_item.scale(0.9);
      else hitItems.forEach(function(hit, index){
              hit.item.scale(0.9);
      });
      break;
    default:
      console.log("Unhandled key pressed:", event.keyCode);
  }
};

SelectDrawer.prototype.onItemMoved = function(page_annotator, name, item, event) {
  event.stopPropagation();
  item.position.x += event.delta.x;
  item.position.y += event.delta.y;
};

var ColorPickerDrawer = function(annotator, previous_mode) {
  BaseDrawer.apply(this, arguments);
  this.annotator.showColorPicker();
};
ColorPickerDrawer.prototype = Object.create(BaseDrawer.prototype);

ColorPickerDrawer.prototype.destroy = function() {
  this.annotator.hideColorPicker();
};

ColorPickerDrawer.prototype.onClick = function(page_annotator, event) {
  this.annotator.setDrawMode(this.previous_mode);
};

var PageAnnotator = function(annotator, pagenum, container, page) {
  this.annotator = annotator;
  this.pagenum = pagenum;
  this.container = container;
  this.page = page;
  this.rotation = 0;
  this.origrotation = 0;
  this.pending_activation = {};
  this.canvas = document.createElement("canvas");
  this.canvas.style.position = "absolute";
  this.canvas.style.left = 0;
  this.canvas.style.top = 0;
  this.canvas.style.right = 0;
  this.canvas.style.bottom = 0;
  this.scope = new paper.PaperScope();
  this.scope.setup(this.canvas);
  if (page) {
    this.setPage(page, container);
  }
  this.view = this.scope.getView();
  this.view.on('mousedown', function() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift(this);
    this.activate();
    this.annotator.drawer.onMouseDown.apply(this.annotator.drawer, args);
  }.bind(this));
  this.view.on('mouseup', function() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift(this);
    this.activate();
    this.annotator.drawer.onMouseUp.apply(this.annotator.drawer, args);
  }.bind(this));
  this.view.on('mousedrag', function() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift(this);
    this.activate();
    this.annotator.drawer.onMouseDrag.apply(this.annotator.drawer, args);
  }.bind(this));
  this.view.on('mouseenter', function() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift(this);
    this.activate();
    this.annotator.drawer.onMouseEnter.apply(this.annotator.drawer, args);
  }.bind(this));
  this.view.on('mouseleave', function() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift(this);
    this.activate();
    this.annotator.drawer.onMouseLeave.apply(this.annotator.drawer, args);
  }.bind(this));
  // Sometimes the "mouseleave" event of paper.js doesn't fire when
  // switching views, also handle "mouseleave" on the canvas itself
  // as a workaround.
  $(this.canvas).on('mouseleave', function() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift(this);
    this.activate();
    this.annotator.drawer.onMouseLeave.apply(this.annotator.drawer, args);
  }.bind(this));
  this.view.on('mousemove', function() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift(this);
    this.activate();
    this.annotator.drawer.onMouseMove.apply(this.annotator.drawer, args);
  }.bind(this));
  this.view.on('click', function() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift(this);
    this.activate();
    this.annotator.drawer.onClick.apply(this.annotator.drawer, args);
  }.bind(this));

  this.delta = 0;
  this.maxtouches = 0;
  this.moved = false;
  this.maybeLongPress = false;
  this.startpoint = null;
  this.startX = 0;
  this.startY = 0;
  this.hitItems = null;
  var pageAnnotator = this;

  this.canvas.addEventListener('touchmove', function (ev){
        pageAnnotator.moved = true;

	if (pageAnnotator.annotator.draw_mode == "select")
                ev.stopPropagation(); // prevent paper.js from eating the event.

	if (pageAnnotator.maybeLongPress && ev.targetTouches.length == 1){
		var dx = Math.abs(ev.targetTouches[0].clientX - pageAnnotator.startX);
		var dy = Math.abs(ev.targetTouches[0].clientY - pageAnnotator.startY);
		var delta = Math.max(dx, dy); // faster than Math.sqrt
		console.log("motion delta: ", delta);
		if (delta > 5) pageAnnotator.maybeLongPress = false;
	}

	if (ev.targetTouches.length == 1 && pageAnnotator.annotator.drawer.selected_item != null && pageAnnotator.hitItems != null){
		var rect = pageAnnotator.canvas.getBoundingClientRect();
		if (pageAnnotator.hitItems.length < 1){
			pageAnnotator.annotator.drawer.selected_item.position.x = (ev.targetTouches[0].clientX - rect.left) / pageAnnotator.scale;
			pageAnnotator.annotator.drawer.selected_item.position.y = (ev.targetTouches[0].clientY - rect.top) / pageAnnotator.scale;
		} else pageAnnotator.hitItems.forEach(function(hit, index){
			hit.item.position.x = (ev.targetTouches[0].clientX - rect.left) / pageAnnotator.scale;
			hit.item.position.y = (ev.targetTouches[0].clientY - rect.top) / pageAnnotator.scale;
		});
	} else if (ev.targetTouches.length >= 2){
                ev.preventDefault(); // prevent browser from zooming whole screen.
                var dx = ev.targetTouches[0].clientX - ev.targetTouches[1].clientX;
                var dy = ev.targetTouches[0].clientY - ev.targetTouches[1].clientY;
                var newDelta = Math.sqrt(dx*dx + dy*dy);
                if (newDelta > pageAnnotator.delta + 10){
			pageAnnotator.delta = newDelta;
			if (pageAnnotator.annotator.drawer.selected_item != null && pageAnnotator.hitItems != null){
				if (pageAnnotator.hitItems.length < 1) pageAnnotator.annotator.drawer.selected_item.scale(1.1);
				else pageAnnotator.hitItems.forEach(function(hit, index){
					hit.item.scale(1.1);
				});
			} else
				PDFViewerApplication.pdfViewer.currentScaleValue = PDFViewerApplication.pdfViewer.currentScale * 1.05;
                } else if (newDelta < pageAnnotator.delta - 10){
			pageAnnotator.delta = newDelta;
			if (pageAnnotator.annotator.drawer.selected_item != null && pageAnnotator.hitItems != null){
                                if (pageAnnotator.hitItems.length < 1) pageAnnotator.annotator.drawer.selected_item.scale(0.9);
                                else pageAnnotator.hitItems.forEach(function(hit, index){
                                        hit.item.scale(0.9);
                                });
			} else
				PDFViewerApplication.pdfViewer.currentScaleValue = PDFViewerApplication.pdfViewer.currentScale / 1.05;
                }
        }
  }, true);

  this.canvas.addEventListener('touchend', function (ev){
        if (pageAnnotator.maxtouches == 1 && !pageAnnotator.moved && pageAnnotator.annotator.draw_mode == "select"){
		pageAnnotator.scope.project.options.hitTolerance = 25;
                var hit = pageAnnotator.scope.project.hitTest(pageAnnotator.startpoint);
                if (!hit)
                        pageAnnotator.annotator.drawer._unselect();
                else
                        pageAnnotator.annotator.drawer._select(hit.item);
        }
        pageAnnotator.moved = false;
        pageAnnotator.maxtouches = 0;
	pageAnnotator.hitItems = null;
	pageAnnotator.maybeLongPress = false;
  }, true);

  this.canvas.addEventListener('auxclick', function (ev){
    ev.preventDefault();
    console.log("middle clicked", pageAnnotator.pagenum);

    var data = {
      "page": "'" + pageAnnotator.pagenum + "'"
    };

    console.log("POST data: ", data);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', "../addpage/" + pageAnnotator.annotator.id, true);
    xhr.onreadystatechange = function(){
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200){
                    console.log("Page added to manipulation project");
                    $("#addInProgress").hide();
                    $("#addSuccess").show();
                    setTimeout(function() {
                      $("#addSuccess").hide();
                    }, 2000);
            } else if (this.readyState === XMLHttpRequest.DONE){
                    console.log("ERROR adding page");
                    $("#addInProgress").hide();
                    $("#addFailed").show();
                    setTimeout(function() {
                      $("#addFailed").hide();
                    }, 2000);
            }
    }
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(data));
    $("#addInProgress").show();

  }, false);

  $(window).keyup(function(event) {
    if (PageAnnotator.prototype.__active !== this) {
      return;
    }

    var args = Array.prototype.slice.call(arguments);
    args.unshift(this);
    this.activate();
    this.annotator.drawer.onKeyUp.apply(this.annotator.drawer, args);
  }.bind(this));
};

PageAnnotator.prototype.activate = function() {
  PageAnnotator.prototype.__active = this;
  for (var drawerName in this.pending_activation) {
    if (!this.pending_activation.hasOwnProperty(drawerName)) {
      continue;
    }

    this.pending_activation[drawerName].activate(this);
  }
  this.pending_activation = {};
  this.scope.activate();
};

PageAnnotator.prototype.registerPendingActivate = function(drawer) {
  this.pending_activation[drawer.name] = drawer;
};

PageAnnotator.prototype.unregisterPendingActivate = function(drawer) {
  delete this.pending_activation[drawer.name];
};

PageAnnotator.prototype.setPage = function(page, container) {
  this.container = container;
  this.page = page;
  this.origrotation = page.rotate;
  var pagesize = page.view;
  if (page.rotate == 0 || page.rotate == 180){
    this.pagewidth = (pagesize[2] - pagesize[0]) * CSS_UNITS;
    this.pageheight = (pagesize[3] - pagesize[1]) * CSS_UNITS;
  } else {
    this.pageheight = (pagesize[2] - pagesize[0]) * CSS_UNITS;
    this.pagewidth = (pagesize[3] - pagesize[1]) * CSS_UNITS;
  }
  if (this.container) {
    this.container.append(this.canvas);
    var canvas = this.canvas;
    var pageAnnotator = this;
    this.canvas.parentElement.addEventListener('touchstart', function (e){
        var rect = canvas.getBoundingClientRect();
	pageAnnotator.maybeLongPress = false;

        if (pageAnnotator.annotator.draw_mode == "select" && pageAnnotator.annotator.drawer.selected_item == null)
                e.stopPropagation(); // prevent paper.js from eating the event.

	if (e.targetTouches.length == 1
		&& pageAnnotator.annotator.draw_mode == "select"
		&& pageAnnotator.annotator.drawer.selected_item != null)
		    pageAnnotator.hitItems = pageAnnotator.scope.project.hitTestAll(pageAnnotator.annotator.drawer.selected_item.position);

        if (e.targetTouches.length > pageAnnotator.maxtouches)
                pageAnnotator.maxtouches = e.targetTouches.length;

        if (e.targetTouches.length == 1 && pageAnnotator.annotator.draw_mode == "select"){
		pageAnnotator.startX = e.touches[0].clientX;
		pageAnnotator.startY = e.touches[0].clientY;
		pageAnnotator.maybeLongPress = true;
                setTimeout(function() {
			if (pageAnnotator.maybeLongPress){
				pageAnnotator.maybeLongPress = false;
				window.navigator.vibrate(20);
				console.log("long press detected");

				var data = {
					"page": "'" + pageAnnotator.pagenum + "'"
				};

				console.log("POST data: ", data);

				var xhr = new XMLHttpRequest();
				xhr.open('POST', "../addpage/" + pageAnnotator.annotator.id, true);
				xhr.onreadystatechange = function(){
					if (this.readyState === XMLHttpRequest.DONE && this.status === 200){
						console.log("Page added to manipulation project");
						$("#addInProgress").hide();
						$("#addSuccess").show();
						setTimeout(function() {
							$("#addSuccess").hide();
						}, 2000);
					} else if (this.readyState === XMLHttpRequest.DONE){
						console.log("ERROR adding page");
						$("#addInProgress").hide();
						$("#addFailed").show();
						setTimeout(function() {
							$("#addFailed").hide();
						}, 2000);
					}
				}
				xhr.setRequestHeader("Content-Type", "application/json");
				xhr.send(JSON.stringify(data));
				$("#addInProgress").show();
			}
                }, 1000);
	}

	if (e.targetTouches.length == 1)
		pageAnnotator.startpoint = new paper.Point((e.touches[0].clientX - rect.left) / pageAnnotator.scale,
			(e.touches[0].clientY - rect.top) / pageAnnotator.scale);
        else if (e.targetTouches.length >= 2) {
                var dx = e.targetTouches[0].clientX - e.targetTouches[1].clientX;
                var dy = e.targetTouches[0].clientY - e.targetTouches[1].clientY;
                pageAnnotator.delta = Math.sqrt(dx*dx + dy*dy);
        }
    }, true);
  }
};

PageAnnotator.prototype.update = function(scale) {
  if (this.container && !this.container.has(this.canvas).length) {
    this.container.append(this.canvas);
  }

  var viewerMain = document.getElementById("viewer");
  var pageContainer = viewerMain.querySelector("div[data-page-number='"+this.pagenum+"']");

  var pwidth = (this.rotation == 90 || this.rotation == 270) ? this.pageheight : this.pagewidth;
  var pheight= (this.rotation == 90 || this.rotation == 270) ? this.pagewidth : this.pageheight;

  this.scale = scale;
  var width = pwidth * scale;
  var height = pheight * scale;

  var cwrapper = pageContainer.querySelector(".canvasWrapper");
  if (cwrapper != null){
    var x = 0, y = 0;
    switch (this.rotation){
    case 90:
        x = -this.pageheight * scale;
        break;
    case 180:
        x = -this.pageheight * scale;
        y = -this.pagewidth * scale;
        break;
    case 270:
        y = -this.pagewidth * scale;
        break;
    }
    cwrapper.style.transform = "rotate("+this.rotation+"deg) translate("+y+"px, "+x+"px)";
    cwrapper.style.transformOrigin = "0% 0%";
    var rotationStyle = cwrapper.style.transform;
  }

  pageContainer.style.cssText = "width: "+width+"px; height: "+height+"px;";
  this.view.viewSize = new paper.Size(width, height);
  this.view.center = new paper.Point(
    (width + (pwidth * (1 - scale))) / 2,
    (height + (pheight * (1 - scale))) / 2);
  this.view.zoom = scale;
  this.view._needsUpdate = true;
  this.view.requestUpdate();
};

PageAnnotator.prototype.createPath = function(options) {
  var path = new paper.Path(options || {});
  path.onMouseDrag = function(event) {
    this.annotator.drawer.onItemMoved(this, path.name, path, event);
  }.bind(this);
  return path;
};

PageAnnotator.prototype.createRectangle = function(options) {
  var rect = new paper.Path.Rectangle(options || {});
  rect.onMouseDrag = function(event) {
    if (rect.ready) {
      this.annotator.drawer.onItemMoved(this, rect.name, rect, event);
    }
  }.bind(this);
  return rect;
};

PageAnnotator.prototype.createEllipse = function(options) {
  var from = new paper.Point(options.from);
  var to = new paper.Point(options.to);
  var rectangle = new paper.Rectangle(
    from, to
  );
  var ellipse = new paper.Path.Ellipse(rectangle);
  ellipse.strokeColor = options.strokeColor;
  ellipse.strokeWidth = options.strokeWidth;
  ellipse.name = options.name;
  ellipse.onMouseDrag = function(event) {
    if (ellipse.ready) {
      this.annotator.drawer.onItemMoved(this, ellipse.name, ellipse, event);
    }
  }.bind(this);
  return ellipse;
};

PageAnnotator.prototype.drawItem = function(name, data) {
  this.activate();
  var path = this.scope.project.getItem({"name": name});
  if (!path) {
    path = this.createPath();
  }
  try {
    path.importJSON(data);
  } catch (e) {
    path.remove();
    path.name = name;
    console.log("Could not import item", data, e);
    return;
  }
  path.name = name;
};

function Annotator(socketurl, id, userid, displayname) {
  this.annotators = {};
  this.cursors = {};
  this.draw_mode = null;
  this.drawer = this.nulldrawer = new NullDrawer();
  this.id = id;
  this.userid = userid;
  this.displayname = displayname;
  this.storage = new Storage('pdfannotate');
  var color = this.storage.get('color');
  if (!color) {
    color = getRandomColor();
    this.storage.set('color', color);
  }
  this.color = color;
  this.pageCount = -1;
  this.currentPage = null;
  this.users = {};
  this.has_document = false;
  this.setDrawMode('select');
  this.pending_messages = [];
  iro.use(iroTransparencyPlugin);
  this.colorPicker = new iro.ColorPicker("#colorPicker", {
    width: 320,
    height: 320,
    color: this.color,
    transparency: true,
  });
  var setColor = function(color) {
    this.color = color.hex8String;
    this.storage.set('color', this.color);
    $(".modeButton.colorMode, .modeButton.colorMode:focus")
      .css("background-color", this.color);
  }.bind(this);
  this.colorPicker.on("color:init", setColor);
  this.colorPicker.on("color:change", setColor);

  this.userlist = $("<div class='userlist'></div>");
  $("#mainContainer").append(this.userlist);
  this.connectionError = $("#connectionError");
  this.connectingMessage = $("#connectingMessage");
}

Annotator.prototype.showColorPicker = function() {
  $("#colorPicker").show();
};

Annotator.prototype.hideColorPicker = function() {
  $("#colorPicker").hide();
};

function compareEntries(a, b) {
  // Compare by display name.
  a = a[1].toLocaleLowerCase();
  b = b[1].toLocaleLowerCase();
  return a.localeCompare(b);
}

Annotator.prototype.documentLoaded = function(pdfDocument) {
  this.pageCount = pdfDocument.numPages;

  this.has_document= true;
  while (this.pending_messages.length) {
    var message = this.pending_messages.shift();
    this.onMessage(message);
  }
};

Annotator.prototype.getExistingPage = function(pagenum) {
  if (typeof(pagenum) === "string") {
    pagenum = parseInt(pagenum, 10);
  }
  if (!this.annotators.hasOwnProperty(pagenum)) {
    return null;
  }

  return this.annotators[pagenum];
};

Annotator.prototype.getPage = function(pagenum, page, container) {
  if (typeof(pagenum) === "string") {
    pagenum = parseInt(pagenum, 10);
  }
  return new Promise(function(resolve, reject) {
    var page_annotator;
    if (!this.annotators.hasOwnProperty(pagenum)) {
      page_annotator = this.annotators[pagenum] = new PageAnnotator(this, pagenum, container, page);
    } else {
      page_annotator = this.annotators[pagenum];
    }

    if (page_annotator.page && page_annotator.container) {
      return resolve(page_annotator);
    } else if (page && container) {
      page_annotator.setPage(page, container);
      return resolve(page_annotator);
    }
    if (page_annotator.page || !PDFViewerApplication.pdfViewer.pdfDocument) {
      return resolve(page_annotator);
    }

    PDFViewerApplication.pdfViewer.pdfDocument.getPage(pagenum).then(function(page) {
      page_annotator.setPage(page, page_annotator.container);
      page_annotator.update(PDFViewerApplication.pdfViewer.currentScale);
      return resolve(page_annotator);
    }, function(error) {
      return reject(error);
    });
  }.bind(this));
};

Annotator.prototype.destroyDrawer = function() {
  if (this.drawer && this.drawer !== this.nulldrawer) {
    var drawer = this.drawer;
    this.drawer = this.nulldrawer;
    drawer.destroy();
  }
};

Annotator.prototype.updateDrawer = function(previous_mode) {
  this.destroyDrawer();
  switch (this.draw_mode) {
    case "freehand":
      this.drawer = new FreehandDrawer(this, previous_mode);
      break;
    case "rectangle":
      this.drawer = new RectangleDrawer(this, previous_mode);
      break;
    case "ellipse":
      this.drawer = new EllipseDrawer(this, previous_mode);
      break;
    case "rotccw":
      this.drawer = new RotateCCW(this, previous_mode);
      break;
    case "rotcw":
      this.drawer = new RotateCW(this, previous_mode);
      break;
    case "select":
      this.drawer = new SelectDrawer(this, previous_mode);
      break;
    case "color":
      this.drawer = new ColorPickerDrawer(this, previous_mode);
      break;
    case "line":
      this.drawer = new LineDrawer(this, previous_mode);
      break;
    case "text":
      this.drawer = new TextInsert(this, previous_mode);
      break;
    case "boxtext":
      this.drawer = new BoxTextInsert(this, previous_mode);
      break;
    case "check":
      this.drawer = new CheckInsert(this, previous_mode);
      break;
    case "inputby":
      this.drawer = new InputbyInsert(this, previous_mode);
      break;
    case "reviewed":
      this.drawer = new ReviewedInsert(this, previous_mode);
      break;
    case "svg":
      this.drawer = new SVGDrawer(this, previous_mode);
      break;
    case null:
      break;
    default:
      console.log("Unknown draw mode", this.draw_mode);
      return;
  }
};

Annotator.prototype.setDrawMode = function(mode) {
  if (this.draw_mode === mode) {
    if (mode === "color") {
      // Toggle back from color picker.
      this.setDrawMode(this.drawer.previous_mode);
    }
    return;
  }

  var previous_mode = this.draw_mode;
  $(".toolbarButton.selected").removeClass('selected');
  this.draw_mode = mode;
  $("#" + (mode || "none") + "Mode").addClass('selected');
  switch (mode) {
    case "select":
    case "color":
    case null:
      break;
    default:
      $("#drawModeToolbar").addClass('selected');
      break;
  }
  this.updateDrawer(previous_mode);
};

Annotator.prototype.switchPage = function(pagenum) {
  if (pagenum === this.currentPage) {
    return;
  }

  this.currentPage = pagenum;
};

Annotator.prototype.exportSVG = function() {
  return new Promise(function(resolve, reject) {
    var result = [];
    var pages = [];

    var continueExport = function() {
      // Remove trailing empty pages.
      while (result.length && !result[result.length-1]) {
        result.pop();
      }

      // No annotations yet?
      if (!result.length) {
        return resolve(result);
      }

      // Need to keep a last empty page to avoid duplicating the last overlay.
      if (result.length < this.pageCount) {
        var page = pages[result.length + 1];
        if (page) {
          var svg = page.scope.project.exportSVG({
            asString: true
          });
          result.push(svg);
        }
      }
      return resolve(result);
    }.bind(this);

    var remaining = this.pageCount;
    for (var i = 1; i <= this.pageCount; i++) {
      this.getPage(i).then(function(pagenum, page_annotator) {
        pages[pagenum] = page_annotator;
        var svg = null;
        if (page_annotator) {
          svg = page_annotator.scope.project.exportSVG({
            asString: true
          });
        }
        result[pagenum - 1] = svg;
        remaining -= 1;
        if (remaining === 0) {
          continueExport();
        }
      }.bind(this, i));
    }
  }.bind(this));
};

Annotator.prototype.savePdf = function() {
  this.exportSVG().then(function(svg) {
    if (!svg.length) {
      // This will never happen because each page gets an SVG, even without any data in it.
      console.log("Processing error.");
      return;
    }

    console.log("Userid: ", this.userid);
    console.log("Display Name: ", this.displayname);

    var rot = "";
    var forcerot = false;
    for (var i = 1; i <= this.pageCount; i++) {
	if (this.annotators[i].rotation != 0) forcerot = true;
	var page_rot = this.annotators[i].rotation + this.annotators[i].origrotation;
	while (page_rot >= 360) page_rot -= 360;
	while (page_rot < 0) page_rot += 360;
	if (i > 1) rot += ",";
	rot += page_rot;
    }
    console.log("Rot:", rot);

    var data = {
      "data": "'" + svg + "'",
      "userid": this.userid,
      "rotation": rot,
      "forcerot": forcerot
    };

    console.log("POST data: ", data);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', "../save/" + this.id, true);
    xhr.onreadystatechange = function(){
	    if (this.readyState === XMLHttpRequest.DONE && this.status === 200){
		    console.log("File save successful");
		    $("#saveInProgress").hide();
		    $("#saveSuccess").show();
                    setTimeout(function() {
                      $("#saveSuccess").hide();
                    }, 4000);
		    location.reload();
	    } else if (this.readyState === XMLHttpRequest.DONE){
		    console.log("File save ERROR");
		    $("#saveInProgress").hide();
                    $("#saveFailed").show();
                    setTimeout(function() {
                      $("#saveFailed").hide();
                    }, 4000);
	    }
    }
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(data));
    $("#saveInProgress").show();
  }.bind(this));
};

function handleFileSelect(evt) {
	evt.stopPropagation();
	evt.preventDefault();

	var files = evt.dataTransfer.files;
	for (var i = 0, f; f = files[i]; i++) {
		var reader = new FileReader();
		reader.onload = function(e){
			svgStamp = e.target.result;
			$('#svgModePopup').toggleClass('hidden');
		};
		reader.readAsText(f);
	}
}

function handleDragOver(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	evt.dataTransfer.dropEffect = 'copy';
}

function handleFilePicked(evt){
	var reader = new FileReader();
	reader.onload = function(e){
		svgStamp = e.target.result;
		$('#svgModePopup').toggleClass('hidden');
	};
	reader.readAsText(evt.target.files[0]);
}

$(document).ready(function() {
  var fileid = document.getElementsByTagName('head')[0].getAttribute('data-fileid');
  var userid = document.getElementsByTagName('head')[0].getAttribute('data-userid');
  var displayname = document.getElementsByTagName('head')[0].getAttribute('data-displayname');
  var permissions = parseInt(document.getElementsByTagName('head')[0].getAttribute('data-permissions'), 10);
  var viewports = parent.document.getElementsByName("viewport");
  var i;
  for (i = 0; i < viewports.length; i++)
	viewports[i].setAttribute("content", "width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no");

  var can_update = ((permissions & PERMISSION_UPDATE) === PERMISSION_UPDATE);

  var annotator = new Annotator("", fileid, userid, displayname);

  var dropZone = document.getElementById('svgModeContainer');
  dropZone.addEventListener('dragover', handleDragOver, false);
  dropZone.addEventListener('drop', handleFileSelect, false);
  var svgpicker = document.getElementById('svgfilepicker');
  svgpicker.addEventListener('change', handleFilePicked, false);

  $(document).on('pagesloaded', function(event) {
    annotator.documentLoaded(PDFViewerApplication.pdfViewer.pdfDocument);
    if (!can_update) {
      $('#viewer .page[data-page-number!=1]').addClass('hiddenPage');
    }
  });

  $(document).on('pagerendered', function(event) {
    var pagenum = event.detail.pageNumber;
    if (!pagenum) {
      console.log("Rendered event without page number", event);
      return;
    }

    var container = $(event.target);
    PDFViewerApplication.pdfViewer.pdfDocument.getPage(pagenum).then(function(page) {
      annotator.getPage(pagenum, page, container).then(function(page_annotator) {
        page_annotator.update(PDFViewerApplication.pdfViewer.currentScale);
      });
    });
  });

  $(document).on('pagechange', function(event) {
    var originalEvent = event.originalEvent;
    if (!originalEvent || typeof(originalEvent.pageNumber) === 'undefined') {
      return;
    }

    var pagenum = originalEvent.pageNumber;
    if (can_update) {
      annotator.switchPage(pagenum);
    } else {
      $('#viewer .page').addClass('hiddenPage');
      var page = $('#viewer .page[data-page-number=' + pagenum +']');
      page.removeClass('hiddenPage');
      // Need to redraw currently visible page to fix any layout issues.
      setTimeout(function() {
        PDFViewerApplication.pdfViewer.update();
      }, 0);
    }
  });

  $(".modeButton").click(function(event) {
    var button = $(event.target);
    annotator.setDrawMode(button.data("mode") || null);
  });

  var $btnDrawMode = $(".toolbarButton.drawMode");
  $btnDrawMode.click(function(event) {
    $('#drawMenuToolbar').toggleClass('hidden');
  });
  $('#drawMenuToolbar .toolbarButton').each(function(_, elem) {
    var $elem = $(elem);
    $elem.click(function() {
      var mode = $(this).data('mode');
      // TODO(leon): This is an ugly hack to determine the previous mode
      $btnDrawMode.get(0).classList.forEach(function(c) {
        if (c !== 'drawMode' && c.indexOf('Mode') !== -1) {
          $btnDrawMode.removeClass(c);
        }
      });
      $btnDrawMode.addClass(mode + 'Mode');
      $('#drawMenuToolbar').addClass('hidden');
    });
  });

  var $btnSVGMode = $(".toolbarButton.svgMode");
  $btnSVGMode.click(function(event){
	  $('#svgModePopup').toggleClass('hidden');
  });

  $("#savePdf").click(function(event) {
    annotator.savePdf();
  });

  $("#secondaryToolbarClose").click(function() {
    history.back();
  });

  if (!can_update) {
    // User may not modify the file.
    $('#outerContainer').addClass('readonly');
    annotator.setDrawMode(null);
  }

  if (history.length <= 1) {
    // Annotation was opened in a new window. Closing windows through JS is not
    // possible - that's why we simply hide the button to not confuse users.
    $("#secondaryToolbarClose").hide();
  }
});

function setupPdfJs() {
  console.log("Loaded pdf.js", pdfjsLib.version, pdfjsLib.build);
  PDFViewerApplicationOptions.set("sidebarViewOnLoad", 0);
  PDFViewerApplicationOptions.set("showPreviousViewOnLoad", false);
  PDFViewerApplicationOptions.set("disablePageMode", true);
  PDFViewerApplicationOptions.set("isEvalSupported", false);
  PDFViewerApplicationOptions.set("cMapUrl", document.getElementsByTagName('head')[0].getAttribute('data-cmapurl'));
  PDFViewerApplicationOptions.set("workerSrc", document.getElementsByTagName('head')[0].getAttribute('data-workersrc'));
}

if (document.readyState === 'interactive' || document.readyState === 'complete') {
  setupPdfJs();
} else {
  document.addEventListener('DOMContentLoaded', setupPdfJs, true);
}
})();
