<?php
$version = \OC::$server->getAppManager->getAppVersion('pdfannotate');
$pages = $_['pages'];
$fileId = $_['fileId'];
?>
<script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>">
document.addEventListener('DOMContentLoaded', function(){
	document.getElementById("exitBtn").addEventListener("click", exitManip);
	document.getElementById("refreshBtn").addEventListener("click", refreshManip);
	document.getElementById("buildBtn").addEventListener("click", function(){saveManip(true);});
	document.getElementById("saveBtn").addEventListener("click", function(){saveManip(false);});
	var x = document.getElementsByClassName("up");
	var i;
	for (i = 0; i < x.length; i++){
		x[i].addEventListener("click", doUp);
	}
	x = document.getElementsByClassName("down");
	        for (i = 0; i < x.length; i++){
                x[i].addEventListener("click", doDown);
	}
        x = document.getElementsByClassName("del");
                for (i = 0; i < x.length; i++){
                x[i].addEventListener("click", doDel);
	}

	var tablewidth = document.getElementById("datatable").getBoundingClientRect().width;
	var pagewidth = document.getElementById("app").getBoundingClientRect().width;
	if (tablewidth < pagewidth / 2){
		document.getElementById("listdiv").style.width = tablewidth + "px";
		document.getElementById("listdiv").style.float = "left";
		document.getElementById("preview").style.width = (pagewidth - tablewidth - 40) + "px";
		document.getElementById("preview").style.float = "right";
	} else {
		document.getElementById("listdiv").style.width = "100%";
		document.getElementById("preview").style.width = "100%";
	}
<?php
foreach($pages as $row)
        echo 'document.getElementById("page_'.$row['page_fileid'].'_'.$row['page'].'").addEventListener("click", genPreview);'."\n";
?>
});

function exitManip(){
	window.history.back();
}

function refreshManip(){
	location.reload();
}

function saveManip(build){
	var data = {
		"fileId": <?php echo '"'.$fileId.'"' ?>,
		"build": build
	};
	var pages = new Array();
	var mRows = document.getElementById("datatable").firstChild;
	var order = 0;
	for (var k = 0; k < mRows.childNodes.length; k++){
		if (mRows.childNodes[k].hasAttribute("id")){
			console.log(mRows.childNodes[k].id);
			pages[order] = mRows.childNodes[k].id;
			order++;
		}
	}
	data['pages'] = pages;
	console.log("Data: ", data);

	var xhr = new XMLHttpRequest();
	xhr.open('POST', "save", true);
	xhr.onreadystatechange = function(){
		if (this.readyState === XMLHttpRequest.DONE && this.status === 200){
			console.log("File save successful");
			$("#saveInProgress").hide();
			$("#saveSuccess").show();
			setTimeout(function() {
				$("#saveSuccess").hide();
			}, 2000);
		} else if (this.readyState === XMLHttpRequest.DONE){
			console.log("File save ERROR");
			$("#saveInProgress").hide();
			$("#saveFailed").show();
			setTimeout(function() {
				$("#saveFailed").hide();
			}, 2000);
		}
	}
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(data));
	$("#saveInProgress").show();
}

function genPreview(){
	console.log(this.id);
	var split = this.id.split("_");
	document.getElementById("preview").innerHTML = '<img style="max-height: 100%; max-width: 100%;" src="../p/'+split[1]+'/'+split[2]+'" />';
	var child = document.getElementById("datatable").firstChild.firstChild;
	while (child = child.nextSibling){
		child.style.backgroundColor = "white";
	}
	this.style.backgroundColor = "yellow";
}

function doUp(e){
	console.log("up clicked");
	var mRow = this.parentNode.parentNode;
	var par = mRow.parentNode;
	var prev = mRow.previousSibling;
	if (prev && prev.id != null && prev.id.length > 0){
		par.removeChild(mRow);
		par.insertBefore(mRow, prev);
	}
	e.stopPropagation();
}

function doDown(e){
	console.log("down clicked");
	var mRow = this.parentNode.parentNode;
	var par = mRow.parentNode;
	var nxt = mRow.nextSibling;
	if (nxt != null && nxt.nextSibling){
		par.removeChild(mRow);
		par.insertBefore(mRow, nxt.nextSibling);
	} else if (nxt != null){
		par.removeChild(mRow);
		par.appendChild(mRow);
	}
	e.stopPropagation();
}

function doDel(e){
	console.log("del clicked on row: "+this.parentNode.parentNode.id);
	this.parentNode.parentNode.remove();
	e.stopPropagation();
}
</script>
<style>
table td, table th {
        padding: 3px;
        word-break: break-all;
}
#saveInProgress { display: none; position: absolute; background: rgba(0, 0, 0, 0.7); left: 50%;
  top: 50%; z-index: 100000; padding: 20px; border-radius: 20px; margin-left: -200px;
  width: 400px; color: white; text-align: center;
}
#saveFailed { display: none; position: absolute; background: rgba(0, 0, 0, 0.7); left: 50%;
  top: 50%; z-index: 100000; padding: 20px; border-radius: 20px; margin-left: -200px;
  width: 400px; color: rgb(255, 38, 0); font-weight: bold; text-align: center;
}
#saveSuccess { display: none; position: absolute; background: rgba(0, 0, 0, 0.7); left: 50%;
  top: 50%; z-index: 100000; padding: 20px; border-radius: 20px; margin-left: -200px;
  width: 400px; color: green; text-align: center;
}
</style>
<div id="app" style="width: 100%;">
	<div id="saveInProgress">Working, please wait...</div>
	<div id="saveFailed">Failed, please try again later.</div>
	<div id="saveSuccess">Success.</div>
	<div id="app-content">
		<div id="headerbar" style="padding: 10px 10px 10px 10px;">
			<button id="exitBtn">Return to files</button>
			<button id="refreshBtn">Refresh</button>
			<button id="buildBtn" title="Create PDF from manipulation project">Build PDF</button>
		</div>
		<div id="contents" style="padding: 10px 10px 10px 10px;">
			<div id="listdiv">
<?php
if (count($pages) > 0){
	echo '<table id="datatable"><tr><th/><th/><th><b>Source</b></th><th><b>Page</b></th><th/></tr>';
	foreach($pages as $row){
		echo '<tr id="page_'.$row['page_fileid'].'_'.$row['page'].'"><td><button class="up" title="Move up">↑</button></td><td><button class="down" title="Move down">↓</button></td>';
		echo '<td>'.$row['name'].'</td><td style="text-align: right;">'.$row['page'].'</td><td><button class="del" title="Remove">×</button></td></tr>';
	}
	echo "</table>";
	echo '<br><button id="saveBtn">Save Changes</button>';
} else {
	echo "No pages found. Please add pages to project.<br>";
	echo "Pages can be added by opening PDF files in this directory and middle clicking on pages to add.";
}
?>
			</div>
			<div id="preview">
		</div>
	</div>
</div>

